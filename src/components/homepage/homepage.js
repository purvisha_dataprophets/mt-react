import React, { useEffect, useState } from 'react';
import './homepage.css';
import { Button, Col, Container, Form, Row, Table, Tabs, Tab, Sonnet } from 'react-bootstrap';
import axios from 'axios';
import Multiselect from 'multiselect-react-dropdown';
import download from 'downloadjs';
const Homepage = ({ updateUser }) => {
    const user = JSON.parse(localStorage.getItem('user'));
    var option = [{ name: 'Option 1️', id: 1 }, { name: 'Option 2️', id: 2 }];
    const [selected, setSelected] = useState([])
    const [options, setOptions] = useState([])
    const [activeTab, setActiveTab] = useState(true)
    const [tab, setTab] = useState({
        upload: false,
        show: false
    })
    const [userList, setUsers] = useState([]);
    const [fileList, setFileList] = useState([]);
    const [permittedUsers, setPermitterUsers] = useState([]);
    const [selectedFile, setSelectedFile] = useState('');

    const headers = {
        'authorization': `Bearer ${user.token}`,
    };

    const getFileList = async () => {
        const res = await axios.get('http://localhost:5000/v1/listFile', { headers });
        setFileList(res.data.files);
    }
    const getUserList = async () => {
        const res = await axios.get('http://localhost:5000/v1/listUser', { headers });
        setUsers(res.data.users);
        let opt = [...options];
        res.data.users.map((el, i) => {
            if (el.name != user.name) {
                opt.push({ name: el.name, id: el._id })
            }
        })
        setOptions(opt);
    }
    useEffect(() => {
        getUserList();
        getFileList();
    }, []);

    const viewFile = async (fileId, fileName, mimetype) => {
        try {
            const res = await axios.get(`http://localhost:5000/v1/fileDetail?userId=${user._id}&fileId=${fileId}`, { responseType: 'blob', headers });
            if (res.data && res.status == 200) {
                const data = res.data;
                download(data, fileName, mimetype);
                return;
            } else {
                alert('sorry!!, you do not have access')
            }
        } catch (err) {
            alert('sorry!!, you do not have access')

        }
    }



    const uploadFile = async (e) => {
        try {
            if (permittedUsers.length) {

                if (userList && userList.length) setPermitterUsers(userList[0]._id);
                else {
                    alert('please select user to give permission');
                    e.preventDefault()
                    return;
                }
            }
            const formData = new FormData();
            formData.append('uploadFile', selectedFile)
            formData.append('permittedUsers', permittedUsers);
            headers['Content-Type'] = 'multipart/form-data';
            const res = await axios.post('http://localhost:5000/v1/uploadFile', formData, { headers });
            if (res.status === 200) {
                alert('file uploaded successfully!!');
            } else {
                alert('something went wrong!!')
            }
        } catch (err) {
            console.log("error")
        }
    }
    const onSelect = (selectedList, selectedItem) => {
        let users = [...permittedUsers];
        users.push(selectedItem.id);
        setPermitterUsers(users);
    }
    const onRemove = (selectedList, removedItem) => {
        let users = [...permittedUsers];
        users.pop(removedItem._id);
        setPermitterUsers(users);
    }

    return (
        <>
            <div className='homepage'>
                <Container className='w-75'>
                    <Row>
                        <Col md={6} style={{ marginRight: "auto" }}>
                            <h1>Hello {user.name}!!</h1>
                        </Col>
                        <Col md={6}>
                            <Button className="pull-right" size="sm" type='button'
                                onClick={() => updateUser({})}>
                                Logout
                            </Button>
                        </Col>
                    </Row>

                    <br></br>
                    <Tabs
                        transition={true}
                        id="noanim-tab-example"
                        className="mb-3"
                        variant="pills"
                    >

                        <Tab eventKey="Upload" title="Upload">
                            <Container className='uploadFile p-4 bgcolor'>
                                <Form onSubmit={uploadFile} encType='multipart/form-data'>
                                    <Row>
                                        <Col>
                                            <h4>
                                                Upload File
                                            </h4>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form.Group controlId="formFile" className="mb-3">
                                                <Form.Control type="file" multiple name='uploadFile' onChange={(e) => {
                                                    setSelectedFile(e.target.files[0])
                                                }} />
                                            </Form.Group>
                                        </Col>
                                        <Col>
                                            <Multiselect
                                                options={options} // Options to display in the dropdown
                                                onSelect={onSelect} // Function will trigger on select event
                                                onRemove={onRemove} // Function will trigger on remove event
                                                displayValue="name" // Property name to display in the dropdown options
                                            />
                                        </Col>
                                        <Col>
                                            <Button type='submit'>Upload</Button>
                                        </Col>
                                    </Row>
                                </Form>
                            </Container>
                        </Tab>
                        <Tab eventKey="Show" title="Show List" onClick={() => getFileList}>
                            <Container className='p-4 bgcolor min-vh-100'>
                                <Table striped bordered hover size="sm" responsive>
                                    <thead>
                                        <tr>
                                            <th>File Path</th>
                                            <th>File Name</th>
                                            <th>Uploaded By</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {fileList && fileList.length ?
                                            fileList.map((item) => (
                                                <tr key={item.id}>
                                                    <td>{item.filePath}</td>
                                                    <td>{item.fileName}</td>
                                                    <td>{item.uploadedBy}</td>
                                                    <td>
                                                        <Button type='button' onClick={() => {
                                                            viewFile(item._id, item.fileName, item.fileMimeType)
                                                        }}> View </Button>
                                                    </td>
                                                    <td />
                                                </tr>
                                            ))
                                            : <tr>
                                                <td colSpan={5} align='center'>
                                                    Please upload file
                                                </td>
                                            </tr>}
                                    </tbody>
                                </Table>
                            </Container>
                        </Tab>
                    </Tabs>
                </Container>
                <br></br>

                <br></br>
            </div>
        </>
    )
}

export default Homepage;